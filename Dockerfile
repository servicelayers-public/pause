FROM golang:alpine AS builder

WORKDIR /workspace

# Install git for go mod
#RUN apk add --no-cache git

# Populate the module cache based on the go.{mod,sum} files.
#COPY go.mod .
#COPY go.sum .

# Because of layer caching, the go mod download
# command will _only_ be re-run when the go.mod or go.sum files change
#RUN go mod download

# Copy source code
COPY . .

# Compile
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o manager .

# distroless provides nobody user:group, ca-certificates, /tmp directory
FROM gcr.io/distroless/static:latest
ENTRYPOINT ["/pause"]
COPY --from=httpd:latest /usr/local/apache2/conf/mime.types /etc/mime.types
COPY --from=builder /workspace/manager /pause
# USER nobody:nobody # ref: http://www.linfo.org/uid.html
USER 65534:65534